package libs

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"gitlab.com/hathi-social/hathi-protocol"
	"net"
	"testing"
	"time"
)

func TestManager(t *testing.T) {
	// Setup
	man, mod := net.Pipe()
	managerEP := protocol.NewEndpointKernel(man)
	moduleEP := protocol.NewEndpointKernel(mod)
	moduleEP.SetImplementedFunctions([]string{"one", "two", "three"})
	manager := NewManager(managerEP)
	// Test Data
	sendOne := &protocol.Packet{Version: 1, RequestID: 1, Function: "one"}
	respOne := &protocol.Packet{Version: 1, RequestID: 1, Function: "result"}
	sendTwo := &protocol.Packet{Version: 1, RequestID: 2, Function: "two"}
	respTwo := &protocol.Packet{Version: 1, RequestID: 2, Function: "result"}
	sendThree := &protocol.Packet{Version: 1, RequestID: 3, Function: "three"}
	respThree := &protocol.Packet{Version: 1, RequestID: 3, Function: "result"}
	// Return chans
	chanOne := make(chan *protocol.Packet, 4)
	chanTwo := make(chan *protocol.Packet, 4)
	chanThree := make(chan *protocol.Packet, 4)
	// Run test
	managerEP.Run()
	moduleEP.Run()
	manager.Start()
	manager.SendPacket(sendOne, chanOne)
	manager.SendPacket(sendTwo, chanTwo)
	time.Sleep(50 * time.Millisecond)
	// Check recieved packet one
	tmp := moduleEP.GetPacket()
	if tmp == nil {
		t.Fatal("First packet not recieved by module")
	} else if tmp.Version != 1 || tmp.RequestID != 1 || tmp.Function != "one" {
		t.Fatal("Incorrect packet (one):", tmp)
	}
	// Check recieved packet two
	tmp = moduleEP.GetPacket()
	if tmp == nil {
		t.Fatal("Second packet not recieved by module")
	} else if tmp.Version != 1 || tmp.RequestID != 2 || tmp.Function != "two" {
		t.Fatal("Incorrect packet (two):", tmp)
	}
	// Module send response to one
	moduleEP.SendPacket(respOne)
	// Check recieved response one
	time.Sleep(50 * time.Millisecond)
	select {
	case tmp = <-chanOne:
		// Check correct response
		if tmp.Version != 1 || tmp.RequestID != 1 || tmp.Function != "result" {
			t.Fatal("Response One incorrect", tmp)
		}
	default:
		t.Fatal("Response One not recieved")
	}
	// Send three and it's response
	manager.SendPacket(sendThree, chanThree)
	moduleEP.SendPacket(respThree) // Note that this is out of order
	// Check recieved response three
	time.Sleep(50 * time.Millisecond)
	select {
	case tmp = <-chanThree:
		// Check correct response
		if tmp.Version != 1 || tmp.RequestID != 3 || tmp.Function != "result" {
			t.Fatal("Response Three incorrect", tmp)
		}
	default:
		t.Fatal("Response Three not recieved")
	}
	// Send response two
	moduleEP.SendPacket(respTwo)
	time.Sleep(50 * time.Millisecond)
	select {
	case tmp = <-chanTwo:
		// Check correct response
		if tmp.Version != 1 || tmp.RequestID != 2 || tmp.Function != "result" {
			t.Fatal("Response Two incorrect", tmp)
		}
	default:
		t.Fatal("Response Two not recieved")
	}
	// Close module side pipe
	mod.Close()
	// Try failing send
	time.Sleep(50 * time.Millisecond)
	err := manager.SendPacket(sendOne, chanOne)
	if err == nil {
		t.Fatal("Failing send didn't fail")
	}
}
