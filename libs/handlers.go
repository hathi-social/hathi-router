package libs

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"gitlab.com/hathi-social/hathi-protocol"
)

// Great Map of Handlers
var ClientHandlers = map[string]func(*protocol.Packet, *routerContext)(bool) {
}

// Handlers

// Handler response waits need to be loops so they can timeout

// TODO: figure out a journalling method for multi-command messages

func login(pkt *protocol.Packet, returnChan chan *protocol.Packet, context *routerContext) (killConnection bool) {
	// send login to IDM
	// wait for response
	// return response to client
	return false
}

func logout(pkt *protocol.Packet, returnChan chan *protocol.Packet, context *routerContext) (killConnection bool) {
	// send logout to IDM
	// wait for response
	// return response to client
	return false
}

func check_session(pkt *protocol.Packet, returnChan chan *protocol.Packet, context *routerContext) (killConnection bool) {
	// send check-session to IDM
	// wait for response
	// return response to client
	return false
}

func new_actor(pkt *protocol.Packet, returnChan chan *protocol.Packet, context *routerContext) (killConnection bool) {
	// send check-session to IDM
	// wait for response
	// if error: return response to client
	// send new-actor to OS
	// wait for response
	// if error: return response to client
	// send add-actor to IDM
	// wait for response
	// if error: send delete-actor to OS
	// return response to client
	return false
}

func delete_actor(pkt *protocol.Packet, returnChan chan *protocol.Packet, context *routerContext) (killConnection bool) {
	// send check-session to IDM
	// wait for response
	// if error: return response to client
	// send delete-actor to OS
	// wait for response
	// if error: return response to client
	// send rm-actor to IDM
	// wait for response
	// return response to client
	return false
}

func get_object(pkt *protocol.Packet, returnChan chan *protocol.Packet, context *routerContext) (killConnection bool) {
	// send check-session to IDM
	// wait for response
	// if error: return response to client
	// send get-object to OS
	// wait for response
	// return response to client
	return false
}

func post_outbox(pkt *protocol.Packet, returnChan chan *protocol.Packet, context *routerContext) (killConnection bool) {
	// send check-session to IDM
	// wait for response
	// if error: return response to client
	// send post-outbox to OS
	// wait for response
	// return response to client
	return false
}
