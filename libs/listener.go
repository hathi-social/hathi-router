package libs

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"time"
	"net"
	"fmt"
	"gitlab.com/hathi-social/hathi-protocol"
)

// Listener loop / spawner function
func Listen(port int, context *routerContext) (err error) {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return err
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			// TODO: log error
		}
		// Have a connection, spawn endpoint and return channel, handoff
		endpoint := protocol.NewEndpointKernel(conn)
		// get all of our handler names
		keys := make([]string, len(ClientHandlers))
		i := 0
		for k := range ClientHandlers {
			keys[i] = k
			i++
		}
		endpoint.SetImplementedFunctions(keys)
		returnChan := make(chan *protocol.Packet, 8) // Void extracted value
		go HandleClient(endpoint, returnChan, context)
	}
	return nil
}

// Client listener function
func HandleClient(endpoint *protocol.EndpointKernel, returnChan chan *protocol.Packet, context *routerContext) {
	for {
		// Get client packet
		pkt := endpoint.GetPacket()
		if pkt == nil {
			if endpoint.IsClosed() {
				// Do we need to do anything special for this?
				break
			}
			// no packet, but still open. Do anti-spin
			time.Sleep(1 * time.Millisecond)
		} else {
			// get packet type handler
			handler, ok := ClientHandlers[pkt.Function]
			if !ok {
				// Can't happen. Handle it anyway.
				// TODO: not-implemented
			}
			kill := handler(pkt, context)
			if kill {
				// TODO: close connection
			}
		}
	}
}
