package libs

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"errors"
	"gitlab.com/hathi-social/hathi-protocol"
	"time"
	//"fmt"
)

type sendingPacket struct {
	Packet     *protocol.Packet
	ReturnChan chan *protocol.Packet
}

func NewManager(endpoint *protocol.EndpointKernel) (mgr *Manager) {
	mgr = &Manager{}
	mgr.running = false
	mgr.incSends = make(chan sendingPacket, 128) // Void Extracted Value
	mgr.returnChans = make(map[int]chan *protocol.Packet, 0)
	mgr.endpoint = endpoint
	return mgr
}

type Manager struct {
	running     bool
	incSends    chan sendingPacket
	returnChans map[int]chan *protocol.Packet
	endpoint    *protocol.EndpointKernel
	requestID   int // counter
}

func (m *Manager) Start() {
	m.running = true
	go m.mainloop()
}

func (m *Manager) Stop() {
	m.running = false
}

func (m *Manager) SendPacket(p *protocol.Packet, returnChan chan *protocol.Packet) (err error) {
	if m.endpoint.IsClosed() {
		return errors.New("")
	} else {
		wrapper := sendingPacket{}
		wrapper.Packet = p
		wrapper.ReturnChan = returnChan
		m.incSends <- wrapper
		return nil
	}
}

func (m *Manager) mainloop() {
	for m.running {
		noPkt := true // no packets this loop
		// Check for packets to send
		select {
		case send := <-m.incSends:
			noPkt = false
			// Have sendable packet, prepare and send it
			m.requestID++
			send.Packet.RequestID = m.requestID
			m.endpoint.SendPacket(send.Packet)
			// Keep the return-chan for the response
			m.returnChans[m.requestID] = send.ReturnChan
		default:
		}
		// Handle received packets
		if pkt := m.endpoint.GetPacket(); pkt != nil {
			noPkt = false
			requestID := pkt.RequestID
			if requestID > 0 { // Response to a packet
				// Get return-chan
				if returnChan, ok := m.returnChans[requestID]; ok {
					delete(m.returnChans, requestID)
					returnChan <- pkt
				} else {
					// Unlinked response; no return-chan.
					// How to handle?
				}
			} else if requestID == 0 { // Unlinked error
			} else { // <0, ping/pong, we should never see this
			}
		} else { // pkt == nil
			if m.endpoint.IsClosed() {
				// How to handle?
			}
		}
		if noPkt {
			// Only do anti-spin when idle
			time.Sleep(5 * time.Millisecond)
		}
	}
}
