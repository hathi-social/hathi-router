
#
# Makefile for hathi-router
#

build:
	go build router.go
test:
	go test ./...
govet:
	go vet ./...
gofmt goformat:
	go fmt ./...
#golint:
